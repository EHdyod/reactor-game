import { defineStore } from "pinia";
import { useStorage  } from '@vueuse/core';

export const useMetricsStore = defineStore("metrics", () => {

    //STATES
    const money = useStorage("money", 0);
    const heat = useStorage("heat", 0);
    const power = useStorage("power", 0);
    const maxHeat = useStorage("maxHeat", 100);
    const maxPower = useStorage("maxPower", 100);

    //PRIVATE ACTIONS
    function initMetrics() {
        money.value = 0;
        heat.value = 0;
        power.value = 0;
        maxHeat.value = 100;
        maxPower.value = 100;
    };
    function addMoney(nb) {
        money.value += nb;
    };
    function removeMoney(nb) {
        money.value -= nb;
    };
    function setMoney(nb) {
        money.value = nb;
    };
    function addHeat(nb) {
        if(heat<maxHeat) heat.value += nb;
    };
    function removeHeat(nb) {
        if(heat.value>0) heat.value -= nb;
    };
    function setHeat(nb) {
        heat.value = nb;
    };
    function addPower(nb) {
        if(power < maxPower) power.value += nb;
    };
    function removePower(nb) {
        if(power.value>0) power.value -= nb;
    };
    function setPower(nb) {
        power.value = nb;
    };
    function setMaxHeat(max) {
        maxHeat.value = max;
    };
    function setMaxPower(max) {
        maxPower.value = max;
    };

    //PUBLIC ACTIONS
    function incrementMoney(){
        addMoney(1);
    };
    function decrementMoney(){
        removeMoney(1);
    };
    function buy(amount){
        removeMoney(amount);
    };
    function sell(amount){
        addMoney(amount);
    };
    function resetMoney(){
        setMoney(0);
    };
    function incrementHeat() {
        addHeat(1);
    };
    function decrementHeat() {
        removeHeat(1);
    };
    function resetHeat() {
        setHeat(0);
    };
    function incrementPower() {
        addPower(1);
    };
    function decrementPower() {
        removePower(1);
    };
    function resetMetrics(){
        console.log("start reset metrics");
        initMetrics();
        console.log("end reset metrics");
    }

    return {money, heat, power, maxHeat, maxPower, incrementMoney, decrementMoney, buy, sell, resetMoney, incrementHeat, decrementHeat, resetHeat, incrementPower, decrementPower, resetMetrics}
});