import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useStorage  } from '@vueuse/core';
import { useMetricsStore } from "./useMetrics";

enum Categories {
    cell,
    vent,
    coolant,
    exchanger,
    plating,
    capacitor
}

class Cell {
    name: string;
    category: Categories;
    tier: number;
    baseCost: number;
    baseTicks: number;
    basePower: number;
    baseHeat: number;

    constructor(name: string = null, category: Categories = null, tier: number = null, baseCost: number = null, baseTicks: number = null, basePower: number = null, baseHeat: number = null) {
        this.name = name;
        this.category = category;
        this.tier = tier;
        this.baseCost = baseCost;
        this.baseTicks = baseTicks;
        this.basePower = basePower;
        this.baseHeat = baseHeat;
    }
}

export const useReactorStore = defineStore("reactor", () => {

    //STATES
    const nbCol = useStorage("nbCol", 9);
    const nbLig = useStorage("nbLig", 9);
    const selectedComponent = ref(null);
    const grid = useStorage("grid", Array(nbLig.value).fill(Array(nbCol.value).fill(new Cell())));
    const nbComponents = useStorage("nbComponents", new Map<string, Number>([
        ["cell", 0],
        ["vent", 0],
        ["coolant", 0],
        ["exchanger", 0],
        ["plating", 0],
        ["capacitor", 0]
    ]));

    //GETTERS
    const cellsHeat = computed(() =>{
        let res = 0;
        grid.value.map(row =>{
            row.map(cell =>{
                if (cell !== null){
                    if(cell["baseHeat"]) res += cell["baseHeat"];
                }
            })
        });
        return res;
    });
    const cellsPower = computed(() =>{
        let res = 0;
        grid.value.map(row =>{
            row.map(cell =>{
                if (cell !== null){
                    if(cell["basePower"]) res += cell["basePower"]
                }
            })
        })
        return res;
    });

    const selectedComp = computed(() =>{
        return selectedComponent;
    });

    function getCell(x, y) {
        return grid.value[x][y];
    };

    //PRIVATE ACTIONS
    function setGrid(newGrid: Cell[][]= null) {
        if (grid.value === null || grid.value.length === 0) {
            grid.value = Array(nbLig.value).fill(Array(nbCol.value).fill(new Cell()));
        }else{
            if(newGrid === null){
                grid.value = Array(nbLig.value).fill(Array(nbCol.value).fill(new Cell()));
            }else{
                newGrid.map((row, ind1) => {
                    row.map((cell, ind2) => {
                        console.log(`will set the cell x:${ind2} y:${ind1} with ${cell}`);
                        grid.value[ind1][ind2] = cell;
                    });
                });
                for(let [key, value] of nbComponents.value){
                    value = 0;
                }
            }
        }
    };
    function setCell({x, y, toFill}) {
        if (toFill){
            grid.value[x][y]= new Cell(selectedComponent.value.name, selectedComponent.value.category, selectedComponent.value.tier, selectedComponent.value.baseCost, selectedComponent.value.baseTicks, selectedComponent.value.basePower, selectedComponent.value.baseHeat);
            nbComponents.value[selectedComponent.value.category]+=1;
        }else{
            nbComponents.value[grid.value[x][y].category]-=1;
            grid.value[x][y]= new Cell();
        }
    };
    function addCol(nb) {
        nbCol.value += nb;
    };
    function removeCol(nb) {
        nbCol.value -= nb;
    };
    function setCol(nb) {
        nbCol.value = nb;
    };
    function addLig(nb) {
        nbLig.value += nb;
    };
    function removeLig(nb) {
        nbLig.value -= nb;
    };
    function setLig(nb) {
        nbLig.value = nb;
    };
    function setSelectedComponent(newComp) {
        selectedComponent.value = newComp;
    };
    function initReactor() {
        Object.keys(nbComponents.value).map(key =>{
            nbComponents.value[key] = 0;
        })
        nbCol.value = 9;
        nbLig.value = 9;
        selectedComponent.value = null;
    };
    function cleanGrid() {
        setGrid();
    };
    function fillGrid(content) {
        setGrid(content);
    };

    //PUBLIC ACTIONS
    function isCellEmpty(x, y) {
        let res = true;
        if(grid.value === null || grid.value.length == 0){
            res = grid.value[x][y] === null;
        }
        return res;
    };
    function emptyCell(x, y) {
        const metrics = useMetricsStore();
        setCell({ x, y, toFill:false});
        if(selectedComponent.value !== null){
            const price = selectedComponent.value["baseCost"];
            metrics.sell(price);
        }
    };
    function fillCell(x, y) {
        const metrics = useMetricsStore();
        if(selectedComponent.value !== null){
            setCell({ x, y, toFill:true});
            const price = selectedComponent.value["baseCost"];
            metrics.buy(price);
        }
    };
    function incrementNbCol() {
        addCol(1);
    };
    function incrementNbLig() {
        addLig(1);
    };
    function selectComp(newC) {
        setSelectedComponent(newC);
    };
    function resetReactor() {
        cleanGrid();
        initReactor();
    };

    return {nbCol, nbLig, selectedComp, grid, cellsHeat, cellsPower, getCell, isCellEmpty, emptyCell, fillCell, incrementNbCol, incrementNbLig, selectComp, resetReactor};
});