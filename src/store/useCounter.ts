import { defineStore } from "pinia";

export const useCounterStore = defineStore("counter", {
    state: () => ({
        counter: 0
    }),
    getters: {
        doubleCount: (state) => {
            return state.counter * 2;
        },
        count: (state) => {
            return state.counter;
        }
    },
    actions: {
        reset() {
            this.counter = 0;
        },
        inc() {
            this.counter += 1;
        },
        dec() {
            this.counter -= 1;
        }
    }
});