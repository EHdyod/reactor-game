module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        "vw-90": "90vw",
        "95": "95%",
      },
      height: {
        "7vh": "7vh",
      },
      flexGrow: {
        '0': 0,
        '1': 1,
        '16': 16
      },
      spacing: {
        'px-5': '5px',
        'px-10': '10px'
      },
      padding: {
        "15": "15px"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
